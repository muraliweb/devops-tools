@echo off

@echo # Start the ZooKeeper service
@echo zookeeper-server-start.bat C:\Apps\kafka_2.13-3.7.0\config\zookeeper.properties
@echo:

@echo # Start the Apache Kafka
@echo kafka-server-start.bat C:\Apps\kafka_2.13-3.7.0\config\server.properties
@echo:

@echo ##### Create the topics
@echo kafka-topics.bat --create --topic onboarding-ai-topic-in --bootstrap-server localhost:9092
@echo:

@echo ##### List all the topics
@echo kafka-topics.bat --list --bootstrap-server localhost:9092
@echo:

@echo ##### Delete the topic
@echo kafka-topics.bat --delete --topic onboarding-ai-topic-in --bootstrap-server localhost:9092
@echo:

@echo ##### Query the topics
@echo kafka-topics.bat --describe --topic onboarding-ai-topic-in --bootstrap-server localhost:9092

