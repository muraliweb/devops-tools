# AWS Security

### Introduction
AWS Security 

### References
[Aamazon Web Services MSK](https://aws.amazon.com/msk/)
[Getting started](https://docs.aws.amazon.com/msk/latest/developerguide/getting-started.html)
[AWS MSK IAM authentication for Sprinboot](https://github.com/aws/aws-msk-iam-auth/blob/main/README.md)
[AWS MSK public access](https://docs.aws.amazon.com/msk/latest/developerguide/public-access.html)
[AWS MSK IAM Access Control](https://docs.aws.amazon.com/msk/latest/developerguide/iam-access-control.html)
[Kafka Properties](https://docs.spring.io/spring-boot/reference/messaging/kafka.html)

### Create an access Key

#### Root access key
![img.png](docs/aws_message_77.png)

![img_1.png](docs/aws_message_78.png)

![img_2.png](docs/aws_message_79.png)

![img_3.png](docs/aws_message_80.png)

![img_4.png](docs/aws_message_81.png)

![img_5.png](docs/aws_message_82.png)

#### IAM user access key
![img_6.png](docs/aws_message_83.png)

![img_7.png](docs/aws_message_84.png)

![img_8.png](docs/aws_message_85.png)

![img_9.png](docs/aws_message_86.png)

### Security in Kafka

```
# Generate a new key and signing request:
# Creates csr.pem and privkey.pem
openssl req -newkey rsa:2048 -nodes -days 3650 -out csr.pem



# Generate the certificate using the generated key and request:
# Creates cert.pem
openssl x509 -req -in csr.pem -out cert.pem -signkey privkey.pem
```

Certificate body -> content of certificate.pem
![img.png](docs/aws_manage_75.png)

Certificate private key -> content of private.key
![img_1.png](docs/aws_manage_76.png)

![img_2.png](docs/aws_manage_77.png)

##### Download logs from EC2 instance
![img_3.png](docs/aws_manage_78.png)

![img_4.png](docs/aws_manage_79.png)

![img_5.png](docs/aws_manage_80.png)

![img_6.png](docs/aws_manage_81.png)

![img_8.png](docs/aws_manage_83.png)

![img_7.png](docs/aws_manage_82.png)

![img_9.png](docs/aws_manage_84.png)

![img_10.png](docs/aws_manage_85.png)

![img_11.png](docs/aws_manage_86.png)

![img_12.png](docs/aws_manage_87.png)

![img_13.png](docs/aws_manage_88.png)

```
# Run scp command
scp -i c:\apps\aws\security\onboarding-ai-key.pem ec2-user@ec2-13-41-79-172.eu-west-2.compute.amazonaws.com:/home/ec2-user/apps/logs/application.log .

```

![img_14.png](docs/aws_manage_89.png)

#### Logs in Kafka
```
2024-07-02 10:47:45.099 [main] DEBUG s.a.m.a.i.i.MSKCredentialProvider - Profile name karunmb

2024-07-02 10:47:48.759 [org.springframework.kafka.KafkaListenerEndpointContainer#0-0-C-1] DEBUG s.a.a.a.c.AwsCredentialsProviderChain - Loading credentials from ProfileCredentialsProvider(profileName=karunmb, profileFile=ProfileFile(sections=[profiles, sso-session], profiles=[Profile(name=default, properties=[aws_access_key_id, aws_secret_access_key]), Profile(name=karunmb, properties=[aws_access_key_id, aws_secret_access_key])]))

2024-07-02 10:47:49.725 [org.springframework.kafka.KafkaListenerEndpointContainer#0-0-C-1] DEBUG s.a.m.a.i.i.MSKCredentialProvider - The identity of the credentials is GetCallerIdentityResponse(UserId=AIDA5F3GIZMQPRF2A5IVA, Account=905915714336, Arn=arn:aws:iam::905915714336:user/karunmb)
```

#### SpringBoot properties
```
spring:
  kafka:
    bootstrap-servers: b-3.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098,b-1.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098,b-2.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098
    properties:
      security:
        protocol: "SASL_SSL"
      sasl:
        mechanism: "AWS_MSK_IAM"
        jaas:
          config: "software.amazon.msk.auth.iam.IAMLoginModule required awsProfileName=karunmb awsRoleArn=\"arn:aws:iam::905915714336:role/onboarding-ai-role\" awsDebugCreds=true awsRoleSessionName=consumer  awsStsRegion=us-west-2;"
        client:
          callback:
            handler:
              class: "software.amazon.msk.auth.iam.IAMClientCallbackHandler"

```