@echo off
echo ====================
echo Kafka CLI Short Cuts
echo ====================
echo kafs -^> kafka-server-start.bat C:\Apps\kafka_2.13-3.7.0\config\server.properties-^> kafs                                    -^> Start Kafka                                       
echo kafz -^> zookeeper-server-start.bat C:\Apps\kafka_2.13-3.7.0\config\zookeeper.properties-^> kafz                                    -^> Start the zookeeper                               
