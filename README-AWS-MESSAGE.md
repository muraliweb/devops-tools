# AWS Messaging

### Introduction
Amazon MSK (Managed streaming Kafka) is the Amazon offered messaging solution

### References
[Aamazon Web Services MSK](https://aws.amazon.com/msk/)
[Getting started](https://docs.aws.amazon.com/msk/latest/developerguide/getting-started.html)
[AWS MSK IAM authentication for Sprinboot](https://github.com/aws/aws-msk-iam-auth/blob/main/README.md)
[AWS MSK public access](https://docs.aws.amazon.com/msk/latest/developerguide/public-access.html)
[AWS MSK IAM Access Control](https://docs.aws.amazon.com/msk/latest/developerguide/iam-access-control.html)
[Kafka Properties](https://docs.spring.io/spring-boot/reference/messaging/kafka.html)

### Steps
##### Select MSK
![img.png](docs/aws_message_1.png)

#### Note the pricing
![img_1.png](docs/aws_message_2.png)

#### Create the Cluster
![img.png](docs/aws_message_3.png)

##### Cluster settings
![img_1.png](docs/aws_message_4.png)

![img_2.png](docs/aws_message_5.png)

<a name="clustersettings"></a>
#### Note the following values
- VPC
  - vpc-2cbb9844
- Subnets
  - subnet-9e88bdf7
  -  subnet-96b037da
  -  subnet-bafc8dc0
- Security groups associated with VPC
  - sg-54e4e52e

![img_3.png](docs/aws_message_6.png)

![img_4.png](docs/aws_message_7.png)

##### Cluster created
![img.png](docs/aws_message_8.png)

#### Create the IAM role
![img_1.png](docs/aws_message_9.png)

![img_2.png](docs/aws_message_10.png)

![img_3.png](docs/aws_message_11.png)

![img_4.png](docs/aws_message_12.png)

![img_5.png](docs/aws_message_13.png)

##### Replace region and Account-ID with correct values
Example JSON
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::905915714336:instance-profile/onboarding-ai-role"
        },
        {
            "Effect": "Allow",
            "Action": [
                "kafka-cluster:Connect",
                "kafka-cluster:AlterCluster",
                "kafka-cluster:DescribeCluster"
            ],
            "Resource": [
                "arn:aws:kafka:eu-west-2:905915714336:cluster/onboarding-ai-cluster/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "kafka-cluster:*Topic*",
                "kafka-cluster:WriteData",
                "kafka-cluster:ReadData"
            ],
            "Resource": [
                "arn:aws:kafka:eu-west-2:905915714336:topic/onboarding-ai-cluster/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "kafka-cluster:AlterGroup",
                "kafka-cluster:DescribeGroup"
            ],
            "Resource": [
                "arn:aws:kafka:eu-west-2:905915714336:group/onboarding-ai-cluster/*"
            ]
        }
    ]
}
```

![img_6.png](docs/aws_message_14.png)

![img_7.png](docs/aws_message_15.png)

![img_8.png](docs/aws_message_17.png)

![img_9.png](docs/aws_message_18.png)

![img_10.png](docs/aws_message_19.png)

![img_11.png](docs/aws_message_20.png)

![img_12.png](docs/aws_message_21.png)

![img_14.png](docs/aws_message_23.png)

![img_15.png](docs/aws_message_24.png)

![img_16.png](docs/aws_message_25.png)

![img_17.png](docs/aws_message_26.png)

#### Create an Amazon EC2 instance with permissions
[README-AWS-DEPLOY.md](README-AWS-DEPLOY.md)

![img.png](docs/aws_message_27.png)

![img_1.png](docs/aws_message_28.png)

![img_2.png](docs/aws_message_29.png)

![img_3.png](docs/aws_message_30.png)

![img_4.png](docs/aws_message_31.png)

![img_5.png](docs/aws_message_32.png)

![img_6.png](docs/aws_message_33.png)

![img_7.png](docs/aws_message_34.png)

![img_8.png](docs/aws_message_35.png)

![img_9.png](docs/aws_message_36.png)

![img_10.png](docs/aws_message_37.png)

![img_11.png](docs/aws_message_38.png)

![img_12.png](docs/aws_message_39.png)

<a name="instancesecuritygroup"></a>
##### Copy the security group name
launch-wizard-8

![img_13.png](docs/aws_message_40.png)

![img_14.png](docs/aws_message_41.png)

![img_15.png](docs/aws_message_42.png)

Note the security group from [Cluster Settings](#clustersettings)
![img_16.png](docs/aws_message_43.png)

![img_17.png](docs/aws_message_44.png)

![img_18.png](docs/aws_message_45.png)

In the 2nd field in Source type the [instance security group](#instancesecuritygroup)
![img_19.png](docs/aws_message_46.png)

![img_20.png](docs/aws_message_47.png)

![img_21.png](docs/aws_message_48.png)

#### Create a topic
![img.png](docs/aws_manage_49.png)

<a name="mskversion"></a>
![img_1.png](docs/aws_manage_50.png)

![img_2.png](docs/aws_manage_51.png)

![img_3.png](docs/aws_manage_52.png)

![img_4.png](docs/aws_manage_53.png)

![img_5.png](docs/aws_manage_54.png)

![img_6.png](docs/aws_manage_55.png)

##### Install Java
```
yum list java * | grep java-17
sudo yum install java-17-amazon-corretto.x86_64
java --version
```
![img_8.png](docs/aws_manage_57.png)

![img_9.png](docs/aws_manage_58.png)

##### Install Kafka
Note the [version of MSK](#mskversion) 
```
# Download the Kafka distribution
wget https://archive.apache.org/dist/kafka/3.5.1/kafka_2.13-3.5.1.tgz

# Extract the Kafka distribution
tar -xzf kafka_2.13-3.5.1.tgz

#Download the Amazon MSK IAM JAR file

cd /home/ec2-user/kafka_2.13-3.5.1/libs
wget https://github.com/aws/aws-msk-iam-auth/releases/download/v1.1.1/aws-msk-iam-auth-1.1.1-all.jar

# Set client properties
cd /home/ec2-user/kafka_2.13-3.5.1/bin
vi client.properties

# Add below properties to client.properties

security.protocol=SASL_SSL
sasl.mechanism=AWS_MSK_IAM
sasl.jaas.config=software.amazon.msk.auth.iam.IAMLoginModule required;
sasl.client.callback.handler.class=software.amazon.msk.auth.iam.IAMClientCallbackHandler
```

![img_10.png](docs/aws_manage_59.png)

![img_11.png](docs/aws_manage_60.png)

![img_12.png](docs/aws_manage_61.png)

![img_13.png](docs/aws_manage_62.png)

![img_14.png](docs/aws_manage_63.png)

##### Create the topic
![img_15.png](docs/aws_manage_64.png)

![img_16.png](docs/aws_manmage_65.png)

![img_17.png](docs/aws_manmage_66.png)

![img_18.png](docs/aws_manmage_67.png)

*Copy the connection string for the private endpoint*
- b-1.onboardingaicluster.1xvyvo.c3.kafka.eu-west-2.amazonaws.com:9098
- b-2.onboardingaicluster.1xvyvo.c3.kafka.eu-west-2.amazonaws.com:9098
- b-3.onboardingaicluster.1xvyvo.c3.kafka.eu-west-2.amazonaws.com:9098

```
cd /home/ec2-user/kafka_2.13-3.5.1/bin
./kafka-topics.sh --create --bootstrap-server b-3.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098 --command-config client.properties --replication-factor 3 --partitions 1 --topic onboarding-ai-topic-in
./kafka-topics.sh --create --bootstrap-server b-3.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098 --command-config client.properties --replication-factor 3 --partitions 1 --topic onboarding-ai-topic-out
```
![img.png](docs/aws_manage_68.png)

![img_1.png](docs/aws_manage_69.png)

##### List all the topics
```
cd /home/ec2-user/kafka_2.13-3.5.1/bin
./kafka-topics.sh --list --command-config client.properties --bootstrap-server b-3.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098

```

##### Describe the Kafka topics
```
cd /home/ec2-user/kafka_2.13-3.5.1/bin
./kafka-topics.sh --describe --topic onboarding-ai-topic-in --command-config client.properties --bootstrap-server b-3.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098
./kafka-topics.sh --describe --topic onboarding-ai-topic-out --command-config client.properties --bootstrap-server b-3.onboardingaicluster.960rvx.c3.kafka.eu-west-2.amazonaws.com:9098
```
kafka-topics.bat --describe --topic onboarding-ai-topic-in --bootstrap-server localhost:9092


##### Edit public access to the MSK broker
[Edit public access](https://docs.aws.amazon.com/msk/latest/developerguide/public-access.html)

![img.png](docs/aws_message_70.png)

![img_1.png](docs/aws_message_71.png)

![img_2.png](docs/aws_message_72.png)

![img_4.png](docs/aws_message_73.png)

*Copy the connection string for the public endpoint*
- b-2-public.onboardingaicluster.1xvyvo.c3.kafka.eu-west-2.amazonaws.com:9198
- b-1-public.onboardingaicluster.1xvyvo.c3.kafka.eu-west-2.amazonaws.com:9198
- b-3-public.onboardingaicluster.1xvyvo.c3.kafka.eu-west-2.amazonaws.com:9198

##### Monitor in CLoud watch
![img_2.png](docs/aws_manage_70.png)

![img_3.png](docs/aws_manage_71.png)

![img_5.png](docs/aws_manage_72.png)

#### Running Kafka locally
[Download Kafka](https://kafka.apache.org/downloads)
[Quick Start](https://kafka.apache.org/quickstart)

##### Set the PATH variable
![img.png](docs/aws_message_76.png)

##### Start the services
```
# Start the ZooKeeper service
zookeeper-server-start.bat C:\Apps\kafka_2.13-3.7.0\config\zookeeper.properties

# Start the Apache Kafka (in a separate terminal)
kafka-server-start.bat C:\Apps\kafka_2.13-3.7.0\config\server.properties
```

##### Create the topics
```
# Run the following commands in a separate terminal
kafka-topics.bat --create --topic onboarding-ai-topic-in --bootstrap-server localhost:9092
kafka-topics.bat --create --topic onboarding-ai-topic-out --bootstrap-server localhost:9092
```
![img.png](docs/aws_message_75.png)

##### List all the topics
```
kafka-topics.bat --list --bootstrap-server localhost:9092
kafka-topics.bat --list --bootstrap-server localhost:9092
```

##### Delete the topic
```
kafka-topics.bat --delete --topic onboarding-ai-topic-in --bootstrap-server localhost:9092
kafka-topics.bat --delete --topic onboarding-ai-topic-out --bootstrap-server localhost:9092
```

##### Query the topics
```
kafka-topics.bat --describe --topic onboarding-ai-topic-in --bootstrap-server localhost:9092
kafka-topics.bat --describe --topic onboarding-ai-topic-out --bootstrap-server localhost:9092
```

##### Number of messages on a topic
```
```

##### Clean up the environment
![img_1.png](docs/aws_message_74.png)



































































