# Feature Flags

This page documents how a server side (back end) application will use LaunchDarkly to implement feature flags.

## About LaunchDarkly feature flags

* References
  * [Get started](https://docs.launchdarkly.com/home/getting-started?q=sdk)
  * [Architecture](https://docs.launchdarkly.com/home/getting-started/architecture) of LaunchDarkly
  * LaunchDarkly [vocabulary](https://docs.launchdarkly.com/home/getting-started/vocabulary)
  * [SDKs](https://docs.launchdarkly.com/sdk)
  * [java-server-sdk](https://github.com/launchdarkly/java-server-sdk) information
  * [hello-java](https://github.com/launchdarkly/hello-java) example guide
  * [Sample](https://reflectoring.io/java-feature-flags/#launchdarkly) implementation
  * [Server side reference implementation](https://docs.launchdarkly.com/sdk/server-side/java)
  * [API](https://launchdarkly.github.io/java-core/lib/sdk/server/) documentation
  * [Trouble shooting connectivity issues](https://support.launchdarkly.com/hc/en-us/articles/14710791365019-Error-Error-in-stream-connection-unable-to-find-valid-certification-path-to-requested-target)

## Setting up LaunchDarkly

* [Get started](https://docs.launchdarkly.com/home/getting-started?q=sdk)
* [Create a Key](https://docs.launchdarkly.com/sdk/concepts/client-side-server-side#keys) - SDK key will be used here 

## How it works

<img src="docs/fl_1.png" width="500">
<br>

* Feature flags are delivered via the Content Delivery Network (CDN)
* Applications connect via the SDK 
* Download the feature flags
* Evaluate and apply logic

## How it hangs together

* Application -> SDK -> CDN -> LaunchDarkly : this is the flow
* At startup the application via SDK connects and pulls the flags and evaluates them
* Connectivity is maintained via streaming or polling (based on connectivity costs)
* [Optional connectivity](#launchdarly-optional-features) features
* If connectivity cant be established in 5 seconds [default values](https://docs.launchdarkly.com/home/flags/new#set-default-values) are served - client tries to connect in the back ground
* At launch all the targeting rules are downloaded to client server. In our case the microservice
* Updates on the LaunchDarkly server are streamed to clients i.e. streamed to out microservices
* Targeting rules are evaluated on the client side i.e. on the microservice 
* A default value is passed is the variation request, this is evaluated on the client side (in this case 3rd param) e.g.
  ```
  boolean booleanFlagActive = launchdarklyClient
       .boolVariation("global-boolean-flag", user, false);
  ```
* Client (server side app) connects to the LaunchDarkly server download the targetting rules at startup and evaultes them on the server.
* There is no poll to the LaunchDarkly server for each evaluation.

## Implementing LaunchDarkly within the code

* Launch darkly is offered as a SaaS
* Set up an account on the portal
* Create an SDK KEY
* The SDK Key will be used to access the managed service
* Set up the feature flag in the portal
* Within code read the state of the feature flag
* Switch functionality based on the feature flag

## LaunchDarkly feature flag granularity

* Individuals
* Device types
* Geographic regions
* Infrastructure components
* Operating system versions

## LaunchDarkly terminology

#### Variation
As an example a boolean flag has a variation of true and false. A string flag can have various string or number values.

#### Targeting rules
The decision as to which variation to use for a user. In the simplest case the targetting rule will decide to show a feature (flag == true).

#### Default values
This is the default variation. i.e. by default is the feature_flag true or false by default.

## LaunchDarly optional features

* [Relay Proxy](https://docs.launchdarkly.com/sdk/relay-proxy)
* [Data Export](https://docs.launchdarkly.com/integrations/data-export)

## Implementation details

* [Server side reference implementation](https://docs.launchdarkly.com/sdk/server-side/java)

### Create the account
[Sign up](https://app.launchdarkly.com/signup?redirect=%2F)
<br>
<img src="docs/fl_4.png" width="400">

### Get the SDK key

#### Identify the key type that is to be used
LaunchDarkly [key types](https://docs.launchdarkly.com/sdk/concepts/client-side-server-side#keys)

* [SDK key](https://docs.launchdarkly.com/sdk/concepts/client-side-server-side#sdk-key) - Server-side SDK key - read-only download the full set
* [Mobile key](https://docs.launchdarkly.com/sdk/concepts/client-side-server-side#mobile-key) - Non-JavaScript client side use
* [Client-side ID](https://docs.launchdarkly.com/sdk/concepts/client-side-server-side#client-side-id) - Javas-script client side id

In our case the microservice will use an SDK key.

<img src="docs/fl_3.png" width="500">

### Create a feature flag

<img src="docs/fl_5.png" width="500">
<br>
<img src="docs/fl_6.png" width="500">
<br>
<img src="docs/fl_7.png" width="500">
<br>
<img src="docs/fl_8.png" width="500">
<br>
<img src="docs/fl_9.png" width="500">
<br>
<img src="docs/fl_10.png" width="500">
<br>
<img src="docs/fl_11.png" width="500">
<br>
<img src="docs/fl_12.png" width="500">

### Implementation

#### Maven
SDK
```
<dependency>
  <groupId>com.launchdarkly</groupId>
  <artifactId>launchdarkly-java-server-sdk</artifactId>
  <version>7.0.0</version>
</dependency>
```
OSGi bundle (includes Gson or SLF4j may conflict with app libraries)
```
<dependency>
   <groupId>com.launchdarkly</groupId>
   <artifactId>launchdarkly-java-server-sdk</artifactId>
   <version>7.0.0</version>
   <classifier>all</classifier>
</dependency>
```
#### Java 

##### Imports
```
import com.launchdarkly.sdk.*;
import com.launchdarkly.sdk.server.*;
```

##### Client config
```application.yml```
```
feature-flag:
  sdk_key: "[MASKED]"
```

```
@Configuration
@ConfigurationProperties(prefix = "feature-flag")
@Data
public class FFConnectionConfig {

    private String sdkKey;
}
```

##### Initailize the client
The client attempts to connect to LaunchDarkly as soon as you call the constructor. The constructor will return when it successfully connects, or when the default timeout of five seconds expires, whichever comes first. If the client does not succeed, your feature flags will serve default values, and the client will continue trying to connect in the background.
```
@Configuration
public class FeatureConfig {

    private LDClient ldClient;

    @Bean
    public LDClient launchdarklyClient(FFConnectionConfig ffConConfig) {
        this.ldClient = new LDClient(getSdkKey(ffConConfig));
        return this.ldClient;
    }

    @PreDestroy
    public void destroy() throws IOException {
        this.ldClient.close();
    }

    private String getSdkKey(FFConnectionConfig ffConConfig) {
        String sdkKey = System.getProperty(LD_SDK_KEY);
        if (StringUtils.isEmpty(sdkKey)) {
            sdkKey = ffConConfig.getSdkKey();
        }
        return sdkKey;
    }
}
```

##### Evaluate the feature flag
```
@Service
public class FeatureFlagService {

    private LDClient ldClient;

    public FeatureFlagService(LDClient ldClient) {
        this.ldClient = ldClient;
    }

    public ClientConnectors getConnectors(String clientId) {

        LDContext context = LDContext.builder(clientId).name(clientId).build();
        String connectors = ldClient.stringVariation(FeatureFlags.CLIENT_CONNECTORS_FLAG, context, "Messging");

        return ClientConnectors.builder()
                .clientId(clientId)
                .connectors(Stream.of(connectors.split(",", -1))
                        .collect(Collectors.toList()))
                .build();
    }
}
```
##### Create an endpoint to test the feature flag
```
@GetMapping("/connectors/{clientId}")
public ClientConnectors getConnectors(@PathVariable String clientId) {
  return ffService.getConnectors(clientId);
}
```

##### Shutdown the client

Gracefully shutdown the client.
```
client.close();
```

### Testing the endpoint
eOBAO
<br>
<img src="docs/fl_13.png" width="500">
FinCrimes
<br>
<img src="docs/fl_14.png" width="500">

## Final thoughts

* The documentation available seems to be all from LaunchDarkly
* Very little independent sample code and implementations
* If we simply want boolean flags is this needed?
* Has a cost implication
* There is separation of concern - feature flags can be managed externally
* Simple UI to manage feature flags
* Give granular feature flags - user, region, brand etc.
* Various targeting rules available
  * Percentage rollout - canary releases
  * User / client based 
* Dynamic toggling without the need for restart

## Alternatives

<img src="docs/fl_2.png" width="500">
